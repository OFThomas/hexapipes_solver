# Hexapipes Solver

    /hexapipes/src/routes/index.svelte
is the file to edit as afaik it acts like _main_ in a C/C++ program. The first
thing we need is 

    import Puzzle from "$lib/puzzle/Puzzle.svelte"
which I think adds the Puzzle class (like a div, idk a tag or something).
A puzzle element looks like 
    
    <Puzzle height={4} width={4} 
    tiles={[2, 57, 2, 24, 40, 25, 10, 2, 4, 49, 22, 8, 48, 32, 5, 4]} wrap={false}
    on:solved={()=>hexSolved = true}/>

# Puzzle.svelte
This file, 
    
    /hexapipes/src/lib/puzzle/Puzzle.svelte 
is what we need. It imports the `HexaGrid` class, `setttings`, `controls` and
the `PipesGame` class. The div class `"puzzle"` is defined on __L:166__, 
I don't know what `use:controls={game}` does. The `{#each` is a loop which
iterates over visible tiles and does `controlMode={$settings.controlMode}`.
Maybe that's like a constructor and only set once?


## Tiles
Tiles are an array of integers.

In <code>/hexapipes/src/lib/puzzle/Puzzle.svelte</code> this is the file
where the game object is created. 

L:25 defines a grid obj

    HexaGrid(width, height, wrap) 

and L:26 creates a PipesGame obj

    PipesGame(grid, tiles, savedProgress)

# HexaGrid objects/function?

So, in `/hexapipes/src/lib/puzzle/hexagrid.js` we find the HexaGrid function.
What are those directions lol.

    const EAST = 1;
    const NORTHEAST = 2;
    const NORTHWEST = 4;
    const WEST = 8;
    const SOUTHWEST = 16;
    const SOUTHEAST = 32;

    this.YSTEP = Math.sqrt(3) / 2;

    this.DIRECTIONS = [EAST, NORTHEAST, NORTHWEST, WEST, SOUTHWEST, SOUTHEAST];

You can declare functions inside functions?

## Methods that I don't need

`fixBoxBounds` I don't think is important for me.

`this.zoom` I also don't think is important.

## Methods that I do need

`this.index_to_xy(index)` might be useful __L:215__ 

`this.xy_to_index(x, y)` __L:228__

`this.rc_to_index(r, c)` __L:388__

`this.find_neighbour(index, direction)` __L:256__

`this.rotate(tile, rotations)` 

# PipesGame objects

`/hexapipes/src/lib/puzzle/game.js` contains the code for the `PipesGame` class.
The class takes a `HexaGrid` object, `tiles` and `savedProgress`.

Properties:
- self.grid (HexaGrid obj)
- self.tiles (array of ints)
- self.connections (Map of tile index -> to set of neighbours that it points to) 
- self.tileStates (array of something)
- self.components (Map of tile index -> component that it belongs to)

## Methods I probably need

`self.rotateTile(tileIndex, times)` 
1. checks if solved
2. checks if tile is locked
3. writes to `oldDirections = self.grid.getDirections(tileState.data.tile,tileState.data.rotations)`
4. does the rotation `tileState.rotate(times)`
5. writes to `newDirections = self.grid.getDirections(tileState.data.tile,tileState.data.rotations)`
6. calculates `dirOut = oldDirections.filter(direction => !(newDirections.some(d=>d===direction)))`
7. calculates `dirIn = newDirections.filter(direction => !(oldDirections.some(d=>d===direction)))` 
8. calls `self.handleConnections()` passing `tileIndex, dirOut, dirIn`

I'm going to be honest, I don't understand JS well enough to know what 6. & 7. 
are doing. Ideally to write the solver I don't need to understand this though,
just be able to trigger rotations, using calls to 

    PipesGame.rotateTile(tileindex, times)
for different tiles and different amounts of rotations.

So, what calls `PipeGame.rotateTile()`? It looks like `hexapipes/src/lib/puzzle/controls.js`.

# Controls.js
    hexapipes/src/lib/puzzle/controls.js


# Triggering a MouseEvent from JS

    https://stackoverflow.com/questions/6157929/how-to-simulate-a-mouse-click-using-javascript


# General tips that you'll keep forgetting (probs)

to build 

    npm install 
then to run 
 
    npm run dev

You can use console.log(strings) to debug, just remember to open the console
on the webpage `F12` and navigate to the `console` section. 
